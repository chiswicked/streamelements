// Copyright 2020 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package streamelements

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// ErrUserNotFound is returned when user is not found
var ErrUserNotFound = errors.New("streamelements: user not found")

// Points struct
// {"channel":"jiwnedc2983yrn234nc29834","username":"chiswicked","points":3059,"pointsAlltime":2900,"rank":2}
type Points struct {
	Channel       string `json:"channel"`
	Username      string `json:"username"`
	Points        int32  `json:"points"`
	PointsAllTime int32  `json:"pointsAlltime"`
	Rank          int32  `json:"rank"`
}

// PutPoints struct
// {"channel":"jiwnedc2983yrn234nc29834","username":"chiswicked","amount":3,"newAmount":230,"message":"Added 3 points to user chiswicked"}
type PutPoints struct {
	Channel   string `json:"channel"`
	Username  string `json:"username"`
	Amount    int32  `json:"amount"`
	NewAmount int32  `json:"newAmount"`
	Message   string `json:"message"`
}

// Rank struct
type Rank struct {
	Username string `json:"username"`
	Rank     int32  `json:"rank"`
}

// Client struct
type Client struct {
	jwt  string
	http *http.Client
}

// NewClient func
func NewClient(jwt string) *Client {
	return &Client{
		jwt:  jwt,
		http: &http.Client{Timeout: time.Second * 10},
	}
}

// GetUserRank f
func (tc *Client) GetUserRank(user, channel string) (int32, error) {
	url := fmt.Sprintf("https://api.streamelements.com/kappa/v2/points/%s/%s/rank", channel, user)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "application/json")

	resp, err := tc.http.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	ret := &Rank{}
	err = json.Unmarshal(body, ret)
	if err != nil {
		return 0, err
	}

	if ret.Rank == 0 {
		return 0, ErrUserNotFound
	}

	return ret.Rank, nil
}

// GetUserPoints f
func (tc *Client) GetUserPoints(user, channel string) (int32, error) {
	url := fmt.Sprintf("https://api.streamelements.com/kappa/v2/points/%s/%s", channel, user)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "application/json")

	resp, err := tc.http.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	ret := &Points{}
	err = json.Unmarshal(body, ret)
	if err != nil {
		return 0, err
	}

	if ret.Rank == 0 {
		return 0, ErrUserNotFound
	}

	return ret.Points, nil
}

// PutUserPoints f
func (tc *Client) PutUserPoints(user, channel string, amount int32) (int32, error) {
	url := fmt.Sprintf("https://api.streamelements.com/kappa/v2/points/%s/%s/%v", channel, user, amount)

	req, err := http.NewRequest("PUT", url, nil)
	if err != nil {
		return 0, err
	}

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+tc.jwt)

	resp, err := tc.http.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	ret := &PutPoints{}
	err = json.Unmarshal(body, ret)
	if err != nil {
		return 0, err
	}

	return ret.NewAmount, nil
}
