# StreamElements API
[![Pipeline Status](https://gitlab.com/chiswicked/streamelements/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/streamelements/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/streamelements/badges/master/coverage.svg)](https://gitlab.com/chiswicked/streamelements/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/chiswicked/streamelements)](https://goreportcard.com/report/gitlab.com/chiswicked/streamelements)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
