# Build configuration

GO_BUILD_ENV		:= GO111MODULE=off CGO_ENABLED=0 

# Local targets

.PHONY: clean install test

all: test

clean:
	@echo [clean] removing test and build artifacts
	@$(GO_BUILD_ENV) go clean -testcache ./...
	@rm -f cover.out cover.html

install:
	@echo [install] installing dependencies
	@$(GO_BUILD_ENV) go get -v -t -d ./...

test: clean install
	@echo [test] running unit tests
	@$(GO_BUILD_ENV) go test -v -coverprofile cover.out ./...
	@$(GO_BUILD_ENV) go tool cover -html=cover.out -o cover.html
