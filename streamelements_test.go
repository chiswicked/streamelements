// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package streamelements

import "testing"

func TestNewClient(t *testing.T) {
	if c := NewClient("secret-id"); c.jwt != "secret-id" {
		t.Fail()
	}
}
